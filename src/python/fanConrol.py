#!/usr/bin/python
import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

# GPIO-Pins 

pinList = [5, 6, 13, 19, 16, 26, 20, 21]

# Loop through Pins

for i in pinList:
    GPIO.setup(i, GPIO.OUT)
    GPIO.output(i, GPIO.HIGH)

# Breath cycle
breathCycleLengthSec = 2.0

# Loop

try:
  while True:
    for i in pinList:
        GPIO.output(i, GPIO.HIGH)
    time.sleep(breathCycleLengthSec /2);
    for i in pinList:
        GPIO.output(i, GPIO.LOW)
    time.sleep(breathCycleLengthSec /2);

# End, Should be controlled by UI...
except KeyboardInterrupt:
  print "Interrupted"
  GPIO.cleanup()