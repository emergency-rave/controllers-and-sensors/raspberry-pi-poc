# Initial Version from Junis

import time
import requests
import datetime
import json
from envirophat import light, motion, weather, leds

log = open('werte.log', 'w')
log.write('light\theading\ttemp\tpress\ttime\tevent\n')

failed = ''
count = 0

aTime = []
aTemp = []
aPress = []
aLight = []
aHeading = []
aEvent = []

try:
	while True:
		leds.on()
		time2 = datetime.datetime.now()
		temp = weather.temperature()
		press = weather.pressure(unit='hPa')
		light2 = light.light()
		heading = motion.heading() % 360
		#motion = str(motion.accelerometer())[1:-1].replace(' ', '')

		if temp>26.4:
			print(str(temp) + "\xc2C - Maximaltemperatur ueberschritten!")
			event = "Maximaltemperatur ueberschritten!"
		elif temp<15.3:
			print(str(temp) + "\xc2C - Minimaltemperatur unterschritten!")
			event = "Minimaltemperatur unterschritten!"
		else:
			print(str(temp) + "\xc2C - Temperatur in Ordnung!")
			event = "-"

		log.write('%f\t%f\t%f\t%f\t%s\t%s\n' % (light2, heading, temp, press, time2, event))
		
		aTime.append(time2)
		aTemp.append(temp)
		aPress.append(press)
		aLight.append(light2)
		aHeading.append(heading)
		aEvent.append(event)
		
		count = count + 1
		
		headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
		url = 'https://pharmatrace.org:7443/engine-rest/engine/default/process-definition/4093b979-af9d-11e9-a6cd-0d3d8803a4f3/submit-form'
		url2 = 'http://PCHH016:8084/process-definition/5cc41858-af20-11e9-a6cd-0d3d8803a4f3/submit-form'
		data = """{\n
\t"variables":{\n
\t\t"light":{\n
\t\t\t"value":\""""+str(aLight)+"""\",\n
\t\t\t"type":"String",\n
\t\t\t"valueInfo":{\n
\t\t\t\t"transient":false\n
\t\t\t}\n
\t\t},\n
\t\t"heading":{\n
\t\t\t"value":\""""+str(aHeading)+"""\",\n
\t\t\t"type":"String",\n
\t\t\t"valueInfo":{\n
\t\t\t\t"transient":false\n
\t\t\t}\n
\t\t},\n
\t\t"temp":{\n
\t\t\t"value":\""""+str(aTemp)+"""\",\n
\t\t\t"type":"String",\n
\t\t\t"valueInfo":{\n
\t\t\t\t"transient":false\n
\t\t\t}\n
\t\t},\n
\t\t"press":{\n
\t\t\t"value":\""""+str(aPress)+"""\",\n
\t\t\t"type":"String",\n
\t\t\t"valueInfo":{\n
\t\t\t\t"transient":false\n
\t\t\t}\n
\t\t},\n
\t\t"time":{\n
\t\t\t"value":\""""+str(aTime)+"""\",\n
\t\t\t"type":"String",\n
\t\t\t"valueInfo":{\n
\t\t\t\t"transient":false\n
\t\t\t}\n
\t\t},\n
\t\t"event":{\n
\t\t\t"value":\""""+str(aEvent)+"""\",\n
\t\t\t"type":"String",\n
\t\t\t"valueInfo":{\n
\t\t\t\t"transient":false\n
\t\t\t}\n
\t\t}\n
}
}"""

		if count > 60:
			try:
				r = requests.post(url, data=data, auth=('unlock', 'etherzulu5'), headers=headers)
				print(r)
				r.json
				count = 0

			except:
				print("Fehler bei der Uebertragung!")
				count = 0
				aTime.append(time2)
				aTemp.append(temp)
				aPress.append(press)
				aLight.append(light2)
				aHeading.append(heading)

		leds.off()
		time.sleep(1)

except KeyboardInterrupt:
	try:
		r = requests.post(url, data=data, auth=('unlock', 'etherzulu5'), headers=headers)
		print(r)
		r.json
		count = 0

	except:
		print("Fehler bei der Uebertragung!")
		count = 0
		aTime.append(time2)
		aTemp.append(temp)
		aPress.append(press)
		aLight.append(light2)
		aHeading.append(heading)
	leds.off()
	log.close()
