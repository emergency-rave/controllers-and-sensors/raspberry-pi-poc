from flask import Flask
from uuid import getnode as get_mac


app = Flask(__name__)
@app.route('/')
def index():        
    mac = get_mac()
    return 'Emergency RAVE controller %s' % hex(mac)
if __name__ == '__main__':       
    app.run(debug=True, host='0.0.0.0')