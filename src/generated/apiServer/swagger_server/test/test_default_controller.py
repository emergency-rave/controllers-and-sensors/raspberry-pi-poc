# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.calibration_result import CalibrationResult  # noqa: E501
from swagger_server.models.device_health import DeviceHealth  # noqa: E501
from swagger_server.models.runtime_parameters import RuntimeParameters  # noqa: E501
from swagger_server.models.sensor_readings import SensorReadings  # noqa: E501
from swagger_server.models.treatment_parameters import TreatmentParameters  # noqa: E501
from swagger_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_createcalibrationresult(self):
        """Test case for createcalibrationresult

        Create a calibrationresult
        """
        body = CalibrationResult()
        response = self.client.open(
            '/calibrationresults',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_createdevice_health(self):
        """Test case for createdevice_health

        Create a device-health
        """
        body = DeviceHealth()
        response = self.client.open(
            '/device-health',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_createruntime_parameters(self):
        """Test case for createruntime_parameters

        Create a runtime-parameters
        """
        body = RuntimeParameters()
        response = self.client.open(
            '/runtime-parameters',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_createsensor_readings(self):
        """Test case for createsensor_readings

        Create a sensorReadings
        """
        body = SensorReadings()
        response = self.client.open(
            '/sensorreadings',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_createtreatmentparameters(self):
        """Test case for createtreatmentparameters

        Create a treatmentparameters
        """
        body = TreatmentParameters()
        response = self.client.open(
            '/treatmentparameters',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_deletecalibrationresult(self):
        """Test case for deletecalibrationresult

        Delete a calibrationresult
        """
        response = self.client.open(
            '/calibrationresults/{calibrationresultId}'.format(calibrationresult_id='calibrationresult_id_example'),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_deletesensor_readings(self):
        """Test case for deletesensor_readings

        Delete a sensorReadings
        """
        response = self.client.open(
            '/sensorreadings/{sensorreadingsId}'.format(sensorreadings_id='sensorreadings_id_example'),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_deletetreatmentparameters(self):
        """Test case for deletetreatmentparameters

        Delete a treatmentparameters
        """
        response = self.client.open(
            '/treatmentparameters/{treatmentparametersId}'.format(treatmentparameters_id='treatmentparameters_id_example'),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_getcalibrationresult(self):
        """Test case for getcalibrationresult

        Get a calibrationresult
        """
        response = self.client.open(
            '/calibrationresults/{calibrationresultId}'.format(calibrationresult_id='calibrationresult_id_example'),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_getcalibrationresults(self):
        """Test case for getcalibrationresults

        List All calibrationresults
        """
        response = self.client.open(
            '/calibrationresults',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_getdevice_healths(self):
        """Test case for getdevice_healths

        List All device-healths
        """
        response = self.client.open(
            '/device-health',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_getruntime_parameters(self):
        """Test case for getruntime_parameters

        List All runtime-parameters
        """
        response = self.client.open(
            '/runtime-parameters',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_getsensor_readings(self):
        """Test case for getsensor_readings

        Get a sensorReadings
        """
        response = self.client.open(
            '/sensorreadings/{sensorreadingsId}'.format(sensorreadings_id='sensorreadings_id_example'),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_getsensorreadings(self):
        """Test case for getsensorreadings

        List All sensorreadings
        """
        response = self.client.open(
            '/sensorreadings',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_gettreatmentparameters(self):
        """Test case for gettreatmentparameters

        Get a treatmentparameters
        """
        response = self.client.open(
            '/treatmentparameters/{treatmentparametersId}'.format(treatmentparameters_id='treatmentparameters_id_example'),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_gettreatmentparameters2(self):
        """Test case for gettreatmentparameters2

        List All treatmentparameters
        """
        response = self.client.open(
            '/treatmentparameters',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_updatecalibrationresult(self):
        """Test case for updatecalibrationresult

        Update a calibrationresult
        """
        body = CalibrationResult()
        response = self.client.open(
            '/calibrationresults/{calibrationresultId}'.format(calibrationresult_id='calibrationresult_id_example'),
            method='PUT',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_updatesensor_readings(self):
        """Test case for updatesensor_readings

        Update a sensorReadings
        """
        body = SensorReadings()
        response = self.client.open(
            '/sensorreadings/{sensorreadingsId}'.format(sensorreadings_id='sensorreadings_id_example'),
            method='PUT',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_updatetreatmentparameters(self):
        """Test case for updatetreatmentparameters

        Update a treatmentparameters
        """
        body = TreatmentParameters()
        response = self.client.open(
            '/treatmentparameters/{treatmentparametersId}'.format(treatmentparameters_id='treatmentparameters_id_example'),
            method='PUT',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
