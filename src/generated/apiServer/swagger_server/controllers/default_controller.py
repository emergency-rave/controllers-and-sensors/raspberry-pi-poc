import connexion
import six

from swagger_server.models.calibration_result import CalibrationResult  # noqa: E501
from swagger_server.models.device_health import DeviceHealth  # noqa: E501
from swagger_server.models.runtime_parameters import RuntimeParameters  # noqa: E501
from swagger_server.models.sensor_readings import SensorReadings  # noqa: E501
from swagger_server.models.treatment_parameters import TreatmentParameters  # noqa: E501
from swagger_server import util


def createcalibrationresult(body):  # noqa: E501
    """Create a calibrationresult

    Creates a new instance of a &#x60;calibrationresult&#x60;. # noqa: E501

    :param body: A new &#x60;calibrationresult&#x60; to be created.
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = CalibrationResult.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def createdevice_health(body):  # noqa: E501
    """Create a device-health

    Creates a new instance of a &#x60;device-health&#x60;. # noqa: E501

    :param body: A new &#x60;device-health&#x60; to be created.
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = DeviceHealth.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def createruntime_parameters(body):  # noqa: E501
    """Create a runtime-parameters

    Creates a new instance of a &#x60;runtime-parameters&#x60;. # noqa: E501

    :param body: A new &#x60;runtime-parameters&#x60; to be created.
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = RuntimeParameters.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def createsensor_readings(body):  # noqa: E501
    """Create a sensorReadings

    Creates a new instance of a &#x60;sensorReadings&#x60;. # noqa: E501

    :param body: A new &#x60;sensorReadings&#x60; to be created.
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = SensorReadings.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def createtreatmentparameters(body):  # noqa: E501
    """Create a treatmentparameters

    Creates a new instance of a &#x60;treatmentparameters&#x60;. # noqa: E501

    :param body: A new &#x60;treatmentparameters&#x60; to be created.
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = TreatmentParameters.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def deletecalibrationresult(calibrationresult_id):  # noqa: E501
    """Delete a calibrationresult

    Deletes an existing &#x60;calibrationresult&#x60;. # noqa: E501

    :param calibrationresult_id: A unique identifier for a &#x60;calibrationresult&#x60;.
    :type calibrationresult_id: str

    :rtype: None
    """
    return 'do some magic!'


def deletesensor_readings(sensorreadings_id):  # noqa: E501
    """Delete a sensorReadings

    Deletes an existing &#x60;sensorReadings&#x60;. # noqa: E501

    :param sensorreadings_id: A unique identifier for a &#x60;sensorReadings&#x60;.
    :type sensorreadings_id: str

    :rtype: None
    """
    return 'do some magic!'


def deletetreatmentparameters(treatmentparameters_id):  # noqa: E501
    """Delete a treatmentparameters

    Deletes an existing &#x60;treatmentparameters&#x60;. # noqa: E501

    :param treatmentparameters_id: A unique identifier for a &#x60;treatmentparameters&#x60;.
    :type treatmentparameters_id: str

    :rtype: None
    """
    return 'do some magic!'


def getcalibrationresult(calibrationresult_id):  # noqa: E501
    """Get a calibrationresult

    Gets the details of a single instance of a &#x60;calibrationresult&#x60;. # noqa: E501

    :param calibrationresult_id: A unique identifier for a &#x60;calibrationresult&#x60;.
    :type calibrationresult_id: str

    :rtype: CalibrationResult
    """
    return 'do some magic!'


def getcalibrationresults():  # noqa: E501
    """List All calibrationresults

    Gets a list of all &#x60;calibrationresult&#x60; entities. # noqa: E501


    :rtype: List[CalibrationResult]
    """
    return 'do some magic!'


def getdevice_healths():  # noqa: E501
    """List All device-healths

    Gets a list of all &#x60;device-health&#x60; entities. # noqa: E501


    :rtype: DeviceHealth
    """
    return 'do some magic!'


def getruntime_parameters():  # noqa: E501
    """List All runtime-parameters

    Gets a list of all &#x60;runtime-parameters&#x60; entities. # noqa: E501


    :rtype: List[RuntimeParameters]
    """
    return 'do some magic!'


def getsensor_readings(sensorreadings_id):  # noqa: E501
    """Get a sensorReadings

    Gets the details of a single instance of a &#x60;sensorReadings&#x60;. # noqa: E501

    :param sensorreadings_id: A unique identifier for a &#x60;sensorReadings&#x60;.
    :type sensorreadings_id: str

    :rtype: SensorReadings
    """
    return 'do some magic!'


def getsensorreadings():  # noqa: E501
    """List All sensorreadings

    Gets a list of all &#x60;sensorReadings&#x60; entities. # noqa: E501


    :rtype: List[SensorReadings]
    """
    return 'do some magic!'


def gettreatmentparameters(treatmentparameters_id):  # noqa: E501
    """Get a treatmentparameters

    Gets the details of a single instance of a &#x60;treatmentparameters&#x60;. # noqa: E501

    :param treatmentparameters_id: A unique identifier for a &#x60;treatmentparameters&#x60;.
    :type treatmentparameters_id: str

    :rtype: TreatmentParameters
    """
    return 'do some magic!'


def gettreatmentparameters2():  # noqa: E501
    """List All treatmentparameters

    Gets a list of all &#x60;treatmentparameters&#x60; entities. # noqa: E501


    :rtype: List[TreatmentParameters]
    """
    return 'do some magic!'


def updatecalibrationresult(body, calibrationresult_id):  # noqa: E501
    """Update a calibrationresult

    Updates an existing &#x60;calibrationresult&#x60;. # noqa: E501

    :param body: Updated &#x60;calibrationresult&#x60; information.
    :type body: dict | bytes
    :param calibrationresult_id: A unique identifier for a &#x60;calibrationresult&#x60;.
    :type calibrationresult_id: str

    :rtype: None
    """
    if connexion.request.is_json:
        body = CalibrationResult.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def updatesensor_readings(body, sensorreadings_id):  # noqa: E501
    """Update a sensorReadings

    Updates an existing &#x60;sensorReadings&#x60;. # noqa: E501

    :param body: Updated &#x60;sensorReadings&#x60; information.
    :type body: dict | bytes
    :param sensorreadings_id: A unique identifier for a &#x60;sensorReadings&#x60;.
    :type sensorreadings_id: str

    :rtype: None
    """
    if connexion.request.is_json:
        body = SensorReadings.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def updatetreatmentparameters(body, treatmentparameters_id):  # noqa: E501
    """Update a treatmentparameters

    Updates an existing &#x60;treatmentparameters&#x60;. # noqa: E501

    :param body: Updated &#x60;treatmentparameters&#x60; information.
    :type body: dict | bytes
    :param treatmentparameters_id: A unique identifier for a &#x60;treatmentparameters&#x60;.
    :type treatmentparameters_id: str

    :rtype: None
    """
    if connexion.request.is_json:
        body = TreatmentParameters.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'
